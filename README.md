## [Main link to the Course](https://www.udemy.com/course/django-python/)

- How to create the most important part of any user application
- How to confidently use some of the most in-demand full stack technologies today
- How to create a local development server from scratch
- How to create a brand new Django project with sqlite database
- How to build your own browsable, self documenting REST API
- Handle user registration, login, and status updates in your app with your very own REST API


1.  **Installation**: You can install Django using pip, a package manager for Python. The command to install Django is:

    `pip install django` 

2.  **Creating a new Django project**: After installing Django, you can create a new project by running the following command:

    `django-admin startproject project_name` 

This will create a new directory with the name of the project, containing several files and directories that make up the basic structure of a Django project.

3.  **Creating a new app**: In Django, an app is a self-contained module that can be added to a project to provide specific functionality. You can create a new app by running the following command:

    `python manage.py startapp app_name` 

This will create a new directory with the name of the app, containing several files and directories that make up the basic structure of a Django app.

4.  **Models**: In Django, a model is a Python class that represents a database table. You define models in the `models.py` file of an app. Here's an example of a simple model:

     ```from django.db import models
     class Person(models.Model):
        name = models.CharField(max_length=50)
        age = models.IntegerField()```

6.  **Views**: In Django, a view is a Python function that handles a web request and returns a web response. You define views in the `views.py` file of an app. Here's an example of a simple view:

    ```from django.shortcuts import render
    from django.http import HttpResponse
    
    def hello(request):
        return HttpResponse("Hello, world!")``` 

7.  **URLs**: In Django, URLs are mapped to views using URL patterns. You define URL patterns in the `urls.py` file of an app. Here's an example of a simple URL pattern:

    ```from django.urls import path
    from . import views
    
    urlpatterns = [
        path('hello/', views.hello, name='hello'),
    ]```

8.  **Templates**: In Django, a template is a text file that defines the structure and presentation of a web page. You define templates in the `templates` directory of an app. Here's an example of a simple template:

```
<!DOCTYPE html>
<html>
<head>
    <title>Hello, world!</title>
</head>
<body>
    <h1>Hello, world!</h1>
</body>
</html>
```
These are just a few of the basics of Django. Django provides a rich set of features and tools that enable developers to build complex web applications quickly and efficiently.

8.  **Admin**: Django comes with a built-in admin interface that allows you to manage your app's data without writing any code. You can customize the admin interface by registering your app's models and creating custom admin classes. Here's an example of registering a model in the admin:

    ```from django.contrib import admin
    from .models import Person
    
    admin.site.register(Person)```

9.  **Forms**: In Django, a form is a Python class that defines the fields and validation rules for a web form. You can define forms in the `forms.py` file of an app. Here's an example of a simple form:

    ```from django import forms
    
    class ContactForm(forms.Form):
        name = forms.CharField(max_length=50)
        email = forms.EmailField()
        message = forms.CharField(widget=forms.Textarea)```

10.  **Static files**: In Django, static files are files that are served directly by the web server, such as images, stylesheets, and JavaScript files. You can define static files in the `static` directory of an app. Here's an example of linking to a static file in a template:
```
<!DOCTYPE html>
<html>
<head>
    <title>Hello, world!</title>
    <link rel="stylesheet" type="text/css" href="{% static 'styles.css' %}">
</head>
<body>
    <h1>Hello, world!</h1>
    <img src="{% static 'image.png' %}" alt="An image">
    <script src="{% static 'script.js' %}"></script>
</body>
</html>
```
11.  **Migrations**: In Django, migrations are used to manage changes to your app's database schema over time. You can create migrations using the `makemigrations` command, and apply them using the `migrate` command. Here's an example of creating and applying a migration:

`python manage.py makemigrations app_name`
`python manage.py migrate`

These are just a few more of the basics of Django. As you can see, Django provides a powerful and flexible framework for building web applications.

12.  **Middleware**: In Django, middleware is a way to modify incoming or outgoing requests and responses. Middleware can be used to implement authentication, caching, and other functionality. You can define middleware in the `middleware.py` file of an app. Here's an example of a simple middleware:

```
   class MyMiddleware:
        def __init__(self, get_response):
            self.get_response = get_response
        def __call__(self, request):
            # Do something before the view is called
            response = self.get_response(request)
            # Do something after the view is called
            return response
```

13.  **Authentication**: In Django, authentication is the process of identifying and verifying the identity of a user. Django provides a built-in authentication system that allows users to log in, log out, and reset their passwords. You can enable authentication by adding the `django.contrib.auth` app to your `INSTALLED_APPS` setting.
    
14.  **Testing**: In Django, testing is an important part of the development process. Django provides a built-in testing framework that allows you to write tests for your app's models, views, and forms. You can run tests using the `test` command. Here's an example of a simple test:

```
from django.test import TestCase
from .models import Person
    
    class PersonTestCase(TestCase):
        def test_create_person(self):
            p = Person.objects.create(name='Alice', age=30)
            self.assertEqual(p.name, 'Alice')
            self.assertEqual(p.age, 30)
```
15.  **Internationalization**: In Django, internationalization (i18n) is the process of adapting an app to different languages and cultures. Django provides built-in support for i18n, including translation tools and language-specific formatting. You can enable i18n by adding the `django.middleware.locale.LocaleMiddleware` middleware to your `MIDDLEWARE` setting.

